package main

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"path"
	"reflect"
	"strings"
	"time"

	"github.com/gocolly/colly"
	"github.com/joho/godotenv"
)

// type Source abstracts a news source
type Source struct {
	ID   string
	Name string
	URL  string
}

func (s *Source) isZero() bool {
	return reflect.DeepEqual(s, Source{})
}

// type Page abstracts a page of a news source.
type Page struct {
	ID          string
	Title       string
	URL         string
	SelectorsID string
	SourceID    string
}

func (p *Page) isZero() bool {
	return reflect.DeepEqual(p, Page{})
}

// type Selector contains DOM element selectors for a type Page.
type Selector struct {
	ID           string
	MainCont     string // Main content container
	SectionCont  string // Section container
	ArticlesCont string //Container holding the articles
	Article      string // Article container containing title, url, and thumbnail.
	Title        string
	URL          string
	Thumbnail    string
}

func (s *Selector) isZero() bool {
	return reflect.DeepEqual(s, Page{})
}

// type Article abstracts a news article
type Article struct {
	ID          string
	Title       string
	URL         string
	PublishedAt time.Time
	Thumbnail   string
	Tags        []string
	SourceID    string
	ScrappedAt  time.Time
}

func (a *Article) Store() {
	tgs := strings.Join(a.Tags, ",")
	line := []string{genuid(), a.Title, a.URL, "", a.Thumbnail, tgs, a.SourceID, a.ScrappedAt.String()}
	arcsv, err := os.OpenFile(path.Join(datadir, "articles.csv"), os.O_CREATE|os.O_APPEND|os.O_WRONLY, os.ModePerm)
	check(err)
	defer arcsv.Close()

	w := csv.NewWriter(arcsv)
	err = w.Write(line)
	check(err)
	w.Flush()
}

var srcs []Source

var pgs []Page

var slcs []Selector

var datadir string

func init() {
	err := godotenv.Load()
	check(err)

	// Directory of data files
	datadir = os.Getenv("DATA_DIR")

	// Populating sources, pages, and selectors
	popsrcs()
	poppgs()
	popselecs()
}
func main() {
	for _, p := range pgs {
		src := GetSourceById(p.SourceID)
		if src.isZero() {
			fmt.Printf("Warning: No source found with the id: %s, associated with the page id: %s", p.SourceID, p.ID)
			return
		}
		// Whitelisting source url
		c := colly.NewCollector(
			colly.AllowedDomains(src.URL),
		)
		// Before making request
		c.OnRequest(func(r *colly.Request) {
			log.Println("Visiting", r.URL.String())
		})

		slc := GetSelectorsById(p.SelectorsID)
		if slc.isZero() {
			fmt.Printf("Warning: No selectors found with the id: %s, associated with the page id: %s", p.SelectorsID, p.ID)
			return
		}

		c.OnHTML(slc.MainCont, func(main *colly.HTMLElement) {
			main.ForEach(slc.SectionCont, func(_ int, sec *colly.HTMLElement) {
				sec.ForEach(slc.ArticlesCont, func(i int, alist *colly.HTMLElement) {
					alist.ForEach(slc.Article, func(j int, a *colly.HTMLElement) {
						//Grouped articles
						ga := a.DOM.Find("ul li").Children()
						//Check if it's not a grouped article
						if ga.Length() == 0 {
							ttle := a.DOM.Find(slc.Title).Text()
							if len(ttle) == 0 {
								return
							}
							url, urlfnd := a.DOM.Find(slc.URL).Attr("href")
							if !urlfnd {
								return
							}
							url = strings.Join([]string{"https://", src.URL, url}, "")
							//Checking if article is scrapped
							if isArticleScrapped(url) {
								return
							}
							thmb, _ := a.DOM.Find(slc.Thumbnail).Attr("srcset")
							a := Article{
								ID:         genuid(),
								Title:      ttle,
								SourceID:   src.ID,
								URL:        url,
								Thumbnail:  thmb,
								Tags:       []string{},
								ScrappedAt: time.Now(),
							}

							a.Store()
						}
					})
				})

			})

		})
		c.Visit(p.URL)
	}

}
