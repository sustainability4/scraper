package main

import (
	"encoding/csv"
	"io"
	"os"
	"path"

	"github.com/google/uuid"
)

func isArticleScrapped(url string) bool {
	//return false
	isScrapped := false
	artcsv, err := os.Open(path.Join(datadir, "articles.csv"))
	check(err)

	defer artcsv.Close()

	r := csv.NewReader(artcsv)
	for {
		rec, err := r.Read()
		if err != nil && err != io.EOF {
			check(err)
		}
		if err == io.EOF {
			break
		}
		if rec[2] == url {
			isScrapped = true
			break
		}

	}
	return isScrapped
}

func genuid() string {
	return uuid.New().String()
}
func check(err error) {
	if err != nil {
		panic(err)
	}
}
func popsrcs() {
	srcsv, err := os.Open(path.Join(datadir, "sources.csv"))
	check(err)
	defer srcsv.Close()

	r := csv.NewReader(srcsv)
	r.FieldsPerRecord = 0
	recs, err := r.ReadAll()
	check(err)
	// Discarting first line
	recs = recs[1:]
	for _, rec := range recs {
		src := Source{
			ID:   rec[0],
			Name: rec[1],
			URL:  rec[2],
		}
		srcs = append(srcs, src)
	}
}

func poppgs() {
	pgcsv, err := os.Open(path.Join(datadir, "pages.csv"))
	check(err)
	defer pgcsv.Close()

	r := csv.NewReader(pgcsv)
	r.FieldsPerRecord = 0
	recs, err := r.ReadAll()
	check(err)
	// Discarting first line
	recs = recs[1:]
	for _, rec := range recs {
		pg := Page{
			ID:          rec[0],
			Title:       rec[1],
			URL:         rec[2],
			SelectorsID: rec[3],
			SourceID:    rec[4],
		}
		pgs = append(pgs, pg)
	}
}

func popselecs() {
	slcsv, err := os.Open(path.Join(datadir, "selectors.csv"))
	check(err)
	defer slcsv.Close()

	r := csv.NewReader(slcsv)
	r.FieldsPerRecord = 0
	recs, err := r.ReadAll()
	check(err)
	// Discarting first line
	recs = recs[1:]
	for _, rec := range recs {
		slc := Selector{
			ID:           rec[0],
			MainCont:     rec[1],
			SectionCont:  rec[2],
			ArticlesCont: rec[3],
			Article:      rec[4],
			Title:        rec[5],
			URL:          rec[6],
			Thumbnail:    rec[7],
		}
		slcs = append(slcs, slc)
	}
}

func GetSelectorsById(id string) Selector {
	for _, slc := range slcs {
		if slc.ID == id {
			return slc
		}
	}
	return Selector{}
}

func GetSourceById(id string) Source {
	for _, src := range srcs {
		if src.ID == id {
			return src
		}
	}
	return Source{}
}
