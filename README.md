## Name
A simple scraper to scrape articles from preditermined web pages.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## env
`DATA_DIR`: Directory where files `articles.csv`, `sources.csv`, `pages.csv`, and `selectors.csv` for storing articles, getting source info, preditermined pages to scrape, and html element sectors respectively, are present.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
MIT license.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
